<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.8.1/flowbite.min.css" rel="stylesheet" >
    <title>Food Menu</title>
   
</head>
<body class="bg-gray-100">
    <div class="container mx-auto py-8">
        <div class="flex">
            <!-- Left side: Food Menu -->
            <div class="w-3/4 p-4">
                <div class="flex">
                    <h1 class="text-2xl font-bold mb-4 mr-4">Food Menu</h1>
                    <h1 class="text-2xl mb-4"><a href="/food/create">Add Menu</a></h1>
                </div>

                <div class="grid grid-cols-4 gap-4">
                    @foreach ($foods as $food)
                    <div class="bg-white p-4 rounded-lg shadow">
                        @php
                        $foodImage = asset('storage/' . $food->gambar);
                        $foodNameInitials = strtoupper(substr($food->nama, 0, 2));
                        @endphp
                        <div class="relative w-full h-40 mb-2">
                            <img src="{{ $foodImage }}" alt="{{ $food->nama }}" class="w-full h-full object-cover rounded" onerror="this.style.display='none'; document.getElementById('foodInitials{{ $food->id }}').style.display='block';">
                            <div id="foodInitials{{ $food->id }}" class="hidden absolute inset-0 flex items-center justify-center text-white font-bold text-3xl bg-black opacity-75">{{ $foodNameInitials }}</div>
                        </div>
                        <p class="text-lg font-semibold">{{ $food->nama }}</p>
                        <p class="text-gray-600">{{ $food->harga }}</p>
                        <button class="mt-2 bg-blue-500 hover:bg-blue-600 text-white py-2 px-4 rounded-full" onclick="addToCart('{{ $food->nama }}', {{ $food->harga }})">Tambah</button>
                    </div>
                    @endforeach
                    
                    
                    
                    
                </div>
            </div>

            <!-- Right side: Bill -->
            <div class="w-2/5 p-4">
                <h1 class="text-2xl font-bold mb-4">Bill</h1>
                <div class="bg-white p-4 rounded-lg shadow">
                    <h3 class="text-2xl font-bold mb-4">New Customer</h3>
                    <!-- Bill items will be dynamically added here -->
                    <p>Dine in</p>
                    <div>
                        <table id="tabelbill" class="w-full text-sm text-left text-gray-900">
                            <thead>

                            </thead>
                            <tbody id="table-body">
                                <!-- Table body will be filled with dummy data using JavaScript -->
                            </tbody>
                        </table>
                    </div>
                    <button class="mt-2 bg-green-500 hover:bg-green-600 text-white py-2 px-4 rounded-full " onclick="clearButtonClick()">Clear</button>
                    <button class="mt-2 bg-green-500 hover:bg-green-600 text-white py-2 px-4 rounded-full">Save Bill</button>
                    <button class="mt-2 bg-green-500 hover:bg-green-600 text-white py-2 px-4 rounded-full" onclick="printTable()">Print Bill</button>

                </div>
                <div class="mt-4">
                    <p id="totalbelanja"class="text-lg font-semibold"></p>
                    <button data-modal-target="small-modal" data-modal-toggle="small-modal" class="mt-2 bg-green-500 hover:bg-green-600 text-white py-2 px-4 rounded-full">Checkout</button>
                </div>

            </div>
        </div>
    </div>



    {{-- Modal --}}
    <!-- Small Modal -->
    <div id="small-modal" tabindex="-1" class="fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full">
        <div class="relative w-full max-w-md max-h-full">
            <!-- Modal content -->
            <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                <!-- Modal header -->
                <div class="flex items-center justify-between p-5 border-b rounded-t dark:border-gray-600">
                    <h3 class="text-xl font-medium text-gray-900 dark:text-white">
                        Checkout
                    </h3>
                    <button type="button" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-hide="small-modal">
                        <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                        </svg>
                        <span class="sr-only">Close modal</span>
                    </button>
                </div>
                <!-- Modal body -->
                <div class="p-6">
                    <p id="totalbelanjamodal" class="text-sm font-medium text-gray-900"></p>
                    <label for="small-input" class="text-sm mr-2 font-medium text-gray-900">Masukkan Nominal Uang</label>
                    <input id="inputField" type="text" class="text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 ">
                    <p class="text-sm font-medium text-gray-900">Kembalian Rp <span id="kembalian">0</span></p>
                    
                </div>
                <!-- Modal footer -->
                <div class="flex items-center p-6 space-x-2 border-t border-gray-200 rounded-b ">
                    <button data-modal-hide="small-modal" type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center ">Selesai</button>
                </div>
            </div>
        </div>
    </div>
</body>




<script>

    function addToCart(itemName, price) {
        const existingItem = dummyData.find((item) => item.name === itemName);
        
        if (existingItem) {
            existingItem.jumlah += 1;
        } else {
            dummyData.push({ name: itemName, price: price, jumlah: 1 });
        }

        updateBillTable();
    }

    let totalPrice = 0; // Initialize the total price
        
    let dummyData = [];
    function updateBillTable() {
        // Get a reference to the table body
        const tableBody = document.getElementById('table-body');

        tableBody.innerHTML = '';

        // Loop through the dummy data and populate the table

        dummyData.forEach((data) => {
            const row = document.createElement('tr');

            // Create table cells for each data point
            const itemCell = document.createElement('td');
            itemCell.textContent = data.name;

            const jumlahCell = document.createElement('td');
            jumlahCell.textContent = data.jumlah;

            const hargaCell = document.createElement('td');
            const totalHarga = data.price * data.jumlah;
            hargaCell.textContent = "Rp. " + totalHarga;
            totalPrice += totalHarga; // Add to the total price

            // Append cells to the row
            row.appendChild(itemCell);
            row.appendChild(jumlahCell);
            row.appendChild(hargaCell);

            // Append the row to the table body
            tableBody.appendChild(row);
        });

        document.getElementById('totalbelanja').innerHTML = "Total Rp." + totalPrice
        document.getElementById('totalbelanjamodal').innerHTML = "Total Belanja Rp." + totalPrice

    }

    function calculateKembalian() {
        // Get references to the input field and the kembalian span element
        const inputField = document.getElementById("inputField");
        const kembalianSpan = document.getElementById("kembalian");

        // Set the total price (you can replace this with your actual total price)
        // Replace with your total price

        // Add an event listener to the input field to calculate kembalian
        inputField.addEventListener("input", () => {
            // Get the user's input value as a number
            const inputValue = parseFloat(inputField.value) || 0;

            // Calculate the kembalian (change)
            const kembalianValue = inputValue - totalPrice;

            // Update the kembalian span with the calculated value
            kembalianSpan.textContent = kembalianValue.toFixed(2); // Display with two decimal places
        });
        }

        // Call the function to initialize the event listener
        calculateKembalian();



    const clearButton = document.getElementById('clear-button');

    // Define a function to perform when the button is clicked
    function clearButtonClick() {
        // Perform the desired action here
        // For example, you can clear the content of a specific element or reset some values
        // Here's an example that clears a <div> with the ID 'result-container'
        const tableBody = document.getElementById('table-body');
    
        tableBody.innerHTML = '';
        dummyData = []
    }


    function printTable() {
        var divToPrint = document.getElementById('tabelbill');
        newWin = window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
   }

       
    
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.8.1/flowbite.min.js"></script>
</html>


