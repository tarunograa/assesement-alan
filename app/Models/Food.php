<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $table = 'foods'; // Specify the table name

    protected $fillable = ['nama', 'harga', 'gambar'];
}
