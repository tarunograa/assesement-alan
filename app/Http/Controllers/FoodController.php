<?php

namespace App\Http\Controllers;

use App\Models\Food;
use Illuminate\Http\Request;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $foods = Food::get();

        return view('index', compact('foods'));
        
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $foods = Food::get();
        return view('tambah', compact('foods'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // dd($request);
        //// Validate the request data
        $request->validate([
            'nama' => 'required|string|max:255',
            'harga' => 'required|numeric',
            'gambar' => [ 'file', 'image'],
        ]);

        // Create a new food item

        $request['link_gambar'] = $request->file('gambar')->store('foods_images');
        Food::create([
            'nama' => $request->input('nama'),
            'harga' => $request->input('harga'),
            'gambar' => $request->input('link_gambar'),
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     */
    public function show(Food $food)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        //

        // dd($id);

        $foods = Food::findOrFail($id);

        return view('edit', compact('foods'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        //
        $foods = Food::findOrFail($id);
        $foods->update($request->all());


        return redirect()->route('food.create');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Food $food)
    {
        //

        // dd($food->id);
        $fooddelete = Food::findOrFail($food->id);

        // Delete the food item
        $fooddelete->delete();

        return back();
    }
}
